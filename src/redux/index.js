import store from './store';
import { fetchUser } from './auth/authActions';
export { store, fetchUser };
